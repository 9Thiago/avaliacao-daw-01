const express = require('express')
const app = express()
const fs = require('fs');

app.use(express.urlencoded({ extended: true }))

app.use('/public', express.static(__dirname + '/public'))


app.get("/json/alunos", (req, res) => {

    fs.readFile(__dirname + "/public/json/alunos.json", "utf8", function (err, data) {
        if (err) throw err;

        const alunos = JSON.parse(data);
        res.send(alunos);
    });

});

app.get('/alunos/buscar', (req, res) => {

    fs.readFile(__dirname + "/public/json/alunos.json", "utf8", function (err, data) {
        if (err) throw err;
        let filtro = req.query.filtro
        let alunos = JSON.parse(data);
        alunos = alunos.filter(aluno => aluno.nome.includes(filtro));
        res.send(alunos);
    });

});

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/public/html/index.html")
})

app.get('/eu', (req, res) => {
    res.sendFile(__dirname + "/public/html/eu.html")
})
app.get('/aluno', (req, res) => {
    res.sendFile(__dirname + "/public/html/aluno.html")
})
app.get('/duvida', (req, res) => {
    res.sendFile(__dirname + "/public/html/duvida.html")
})

app.post('/confirmacao', (req, res) => {
    res.send('<h1></h1><p>Sua dúvida: "' + req.body.descricao +
        '" foi enviada com sucesso e será respondida no e-mail: ' + req.body.email + '</p>')
})


console.log('Server is running');
app.listen(3000);